<?php

/**
 * Slot Strategy global data service; provides access to data used globally in all pages,
 * via the Twig global "globalData"
 */

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;

use App\Entity\{Tag, Streamer, Casino};

class GlobalDataService
{
	protected $em;

	protected $viewData = [];

    public function __construct(EntityManagerInterface $em)
    {
    	$this->em = $em;

    	$categories = $this->em->getRepository(Tag::class)->getGameCategories();
    	$streamers = $this->em->getRepository(Streamer::class)->findBy(['active' => true, 'featured' => true], ['displayOrder' => 'ASC']);
    	$casinos = $this->em->getRepository(Casino::class)->findBy(['active' => true, 'featured' => true], ['displayOrder' => 'ASC']);

    	$viewData = [
    		'game_categories' => $categories,
    		'streamers' => $streamers,
    		'casinos' => $casinos,
    	];

    	$this->viewData = $viewData;
    }

    public function getGameCategories()
    {
    	return $this->viewData['game_categories'];
    }

    public function getFeaturedStreamers()
    {
    	return $this->viewData['streamers'];
    }

    public function getFeaturedCasinos()
    {
    	return $this->viewData['casinos'];
    }

}